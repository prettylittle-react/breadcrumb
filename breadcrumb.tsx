import * as React from 'react';

import {Navigation} from 'navigation';

export interface IAction {
	label: string;
	children?: string;
	className?: string;
	isWide?: boolean;
	style?: string;
	size?: string;
	icon?: any;
	iconPrefix?: any;
	iconSuffix?: any;
	onClick?(any):void;
}

export interface ILink extends IAction {
	href: string;
	target?: string;
	role?: string;
	rel?: string;
	isExternal?: boolean;
}

import EventEmitter from 'eventemitter';

import './Breadcrumb.scss';

interface IBreadcrumbItem extends ILink {
    isDynamic? : boolean;
}

interface IProps {
	items: Array<IBreadcrumbItem> | null;
}

interface IState {
	items: Array<IBreadcrumbItem> | null;
}

export default class Breadcrumb extends React.PureComponent<IProps, IState> {

    private base : string = 'breadcrumb';

    private static propTypes : IProps = {
        items : null
    }

	public state : IState = {
		items: this.props.items
    };

    public componentWillReceiveProps(next) {
        this.setState({
            items: next.items
        });
    }

    public componentDidMount(){
        EventEmitter.subscribe('onbreadcrumb', this.onBreadcrumb);
    }

    private onBreadcrumb = (props) => {
        if(!this.state.items)
        {
            return;
        }

        const items = this.state.items.filter((item, index) => {
            return !item.isDynamic;
        });

        this.setState({
            items : [...items, ...props.items]
        })
    }

	public render(): React.ReactNode {
        const {items} = this.state;

        if(!items || items && items.length === 0){
            return null;
        }

		return <div className={this.base}>
            <Navigation items={items} />
        </div>;
	}
}
