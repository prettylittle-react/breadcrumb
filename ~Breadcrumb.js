import React from 'react';
import PropTypes from 'prop-types';

import {Navigation} from 'navigation';

import './Breadcrumb.scss';

/**
 * Breadcrumb
 * @description [Description]
 * @example
  <div id="Breadcrumb"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Breadcrumb, {
        title : 'Example Breadcrumb'
    }), document.getElementById("Breadcrumb"));
  </script>
 */
class Breadcrumb extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'breadcrumb';

		this.state = {items: props.items};
	}

	render() {
		const {items} = this.state;
		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				<Navigation items={items} />
			</div>
		);
	}
}

Breadcrumb.defaultProps = {
	items: null
};

Breadcrumb.propTypes = {
	items: PropTypes.array.isRequired
};

export default Breadcrumb;
